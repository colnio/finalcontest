comm = input()

x = [int(i) for i in comm.split(',')[:3:]]
y = [int(i) for i in comm.split(',')[3:-1:]]

sign = comm.split(',')[-1]
result = [0 for i in range(len(x))]
if sign == '+':
    result = [x[i] + y[i] for i in range(len(x))]

elif sign == '*':
    result[0] = x[1] * y[2] - x[2] * y[1]
    result[1] = - (x[0] * y[2] - x[2] * y[0])
    result[2] = x[0] * y[1] - x[1] * y[0]

print(*result, sep=',')