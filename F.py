def seek_furthest_between_2(arr, l, r):
    m = 0
    tmp = 0
    index = -1
    for i in range(l+1, r):
        tmp = min(arr[i][1] - arr[l][1], arr[r][1] - arr[i][1])
        if tmp > m:
            m = tmp
            index = i
    return [index, m]

def seek_furthest_total(arr, l, r):
        l_temp = l
        local_maxs = []
        for i in range(l + 1, r + 1):
            if arr[i][0] == 'hp' and i - l_temp > 1:
                local_maxs.append(seek_furthest_between_2(arr, l_temp, i))
                l_temp = i
        return max(local_maxs, key=lambda x: x[1])
            



n, k = map(int, input().split())
a = list(map(int, input().split()))

z = [['av', i] for i in a]         # av - avaliable, hp - horse placed, initially no horse placed

z[0][0] = z[-1][0] = 'hp'          # place 2 horses into opposit edges because of the biggest distance
k -= 2
m = z[-1][1]

while k > 0:
    temp = seek_furthest_total(z, 0, len(z) - 1)
    z[temp[0]][0] = 'hp'
    temp_m = temp[1]
    if temp_m < m:
        m = temp_m
    k -= 1

l_temp = 0

print(m)